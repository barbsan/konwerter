#include <iostream>
#include <cmath>

using namespace std;


void displayArray(char* arr, int length) {
	for (int i = 0; i < length; i++) {
		cout << *(arr + i) - 48 << " ";
	}
	cout << endl;
}

char* floatToChar(float f) {
	int exponent, i, index ;
	float temp;
	char* result = new char[33];
	*result = 48;

	if (f < 0.0) {
		(*result)++;
		f *= (-1.0);
	}

	for (i = 0; i < 256; i++) {
		temp = f / pow(2, i - 127);
		if (temp >= 1.0 && temp < 2.0) {
			exponent = i;
			f = temp - 1.0;
			break;
		}
	}

	for (i = 0; i < 8; i++) {
		index = 8 - i;
		*(result + index) = (exponent & 1 << i) ? 49 : 48;
	}
	 
	for (i = 1; i < 25; i++) {
		temp = pow(2, -i);
		index = 8 + i;
		*(result + index) = 48;
		if (temp <= f) {
			(*(result + index))++;
			f -= temp;
		}
	}

	return result;
}

float charToFloat(char* b) {
	float result = 0.0, mantissa = 0.0;
	int sign, i, index, exponent = 0;

	sign =pow((-1),(int)*b);
	
	for (i = 0; i <8; i++) {
		index = 8 - i;
		exponent += (*(b + index)-48)*pow(2, i);
	}
	exponent -= 127;

	for (i = 1; i < 25; i++) {
		index = 8 + i;
		mantissa += (*(b + index)-48)*pow(2, -i);
	}

	cout << "Sign: " << sign << " Exponent: " << exponent << " Mantissa: " << mantissa << endl;
	result = sign* pow(2, exponent)*(1.0+mantissa);
	return result;
}

int main() {
	char *result;
	char sth[33] = "01000000011000000000000000000000";
	float value = charToFloat(sth);
	cout << value << endl;
	result = floatToChar(value);
	displayArray(result, 32);
	result = floatToChar(1.5);
	displayArray(result, 32);
	cout << charToFloat(result) << endl;
	result = floatToChar(5.0);
	displayArray(result, 32);
	cout << charToFloat(result) << endl;
	result = floatToChar(-0.1);
	displayArray(result, 32);
	cout << charToFloat(result) << endl;
	system("pause");
	return 0;
}